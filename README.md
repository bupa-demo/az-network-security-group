azure-terraform-modules

# Usage
Reference the module as below and pass required and/or optional arguments to provision a network security group. This will create a single NSG and and additional rules defined.

```
module "nsg-v1" {
  source = "git@ssh.dev.azure.com:v3/Delu2011/az-terraform-module/az-network-security-group"
  network_security_group_name   = var.network_security_group_name
  resource_group_name           = module.vnet_resource_group.resource_group_name
  location                      = module.vnet_resource_group.resource_group_location
  ingress_rules                 = var.ingress_rules
  egress_rules                  = var.egress_rules
  tags                          = var.default_tags
}
```

# Inputs
| Name | Description | Type | Default | Required |
| :---: | :---: | :---: | :---: | :---: |
| vnet_name | Name of virtual network to be created | string | n/a | yes |
| location | Azure region to use | string | n/a | yes |
| environment | Environment to deploy | string | n/a | no | 
| project | Name of project | string | n/a | no | 
| resource_group_name | Resource group name | string | n/a | yes |
| ingress_rules | Inbound addresses to allow | list(object) | n/a | no |
| egress_rules | Outbound addresses to allow | list(object) | n/a | no |
| tags | Tags to add to resources | map(string) | n/a | no |


# Outputs
| Name | Description |
| :---: | :---: |
| network_security_group_id | Network security group generated id |
| network_security_group_name | Network security group name |

