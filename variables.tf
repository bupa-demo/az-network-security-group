variable "network_security_group_name" {
  description = "Security Group name."
  type        = string
}

variable "location" {
  description = "Azure location."
  type        = string
}

variable "environment" {
  description = "Project environment"
  type        = string
  default     = ""

}

variable "project" {
  description = "Project name"
  type        = string
  default     = ""
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}

variable "ingress_rules" {
  type = list(object({
    name                        = string
    priority                    = number
    access                      = string
    protocol                    = string
    source_port_range           = string
    destination_port_range      = string
    source_address_prefix       = string
    destination_address_prefix  = string
  }))
  default = []
}

variable "egress_rules" {
  type = list(object({
    name                        = string
    priority                    = number
    access                      = string
    protocol                    = string
    source_port_range           = string
    destination_port_range      = string
    source_address_prefix       = string
    destination_address_prefix  = string
  }))
  default = []
}

variable "tags" {
  description = "Extra tags to add."
  type        = map(string)
  default     = {}
}
