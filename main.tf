resource "azurerm_network_security_group" "network_security_group" {
  name                = var.network_security_group_name
  resource_group_name = var.resource_group_name
  location            = var.location
  tags                = var.tags
}

resource "azurerm_network_security_rule" "ingress_rules" {
  for_each                      = { for rule in var.ingress_rules : rule.name => rule }
  name                          = each.value.name
  priority                      = each.value.priority
  direction                     = "Inbound"
  access                        = each.value.access
  protocol                      = each.value.protocol
  source_port_range             = each.value.source_port_range
  destination_port_range        = each.value.destination_port_range
  source_address_prefix         = each.value.source_address_prefix
  destination_address_prefix    = each.value.destination_address_prefix
  resource_group_name           = var.resource_group_name
  network_security_group_name   = azurerm_network_security_group.network_security_group.name
}

resource "azurerm_network_security_rule" "egress_rules" {
  for_each                      = { for rule in var.egress_rules : rule.name => rule }
  name                          = each.value.name
  priority                      = each.value.priority
  direction                     = "Outbound"
  access                        = each.value.access
  protocol                      = each.value.protocol
  source_port_range             = each.value.source_port_range
  destination_port_range        = each.value.destination_port_range
  source_address_prefix         = each.value.source_address_prefix
  destination_address_prefix    = each.value.destination_address_prefix
  resource_group_name           = var.resource_group_name
  network_security_group_name   = azurerm_network_security_group.network_security_group.name
}